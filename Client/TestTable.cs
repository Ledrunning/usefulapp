﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client
{
    class TestTable
    {
     
            public TestTable(int Id, string Vocalist, string Album, int Year)
            {
                this.Id = Id;
                this.Vocalist = Vocalist;
                this.Album = Album;
                this.Year = Year;
            }
            public int Id { get; set; }
            public string Vocalist { get; set; }
            public string Album { get; set; }
            public int Year { get; set; }
    }
}
