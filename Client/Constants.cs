﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client
{
    /// <summary>
    /// Constant class
    /// </summary>
    public static class Constants
    {
        public const short DATAGRID_SIZE = 3;
        public const string RU = " RU";
        public const string US = " US";
        public const string PRINT_ZERO_VALUE = "0";
    }
}
