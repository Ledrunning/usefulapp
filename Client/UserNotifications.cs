﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client
{
    public static class UserNotifications
    {
        public static readonly string ERROR = "Ошибка!";
        public static readonly string NO_ENTRY_SELECTED = "Ни одна запись не выбрана или поле пустое!";
        public static readonly string FILL_ALL_FIELDS = "Заполните поля или нажмите \"Отмена\"!";
    }
}
